import * as AWS from 'aws-sdk';
import * as awsMock from 'aws-sdk-mock';
import { assert } from 'chai';
import * as log from 'econ-logger';
import * as sinon from 'sinon';
import { S3Helper } from '../src/s3';

describe('s3', () => {
  let s3: S3Helper, businessLocationDownload: any, fileName: string;
  const sandbox = sinon.createSandbox();
  const businessLocationBucket = 'my-bucket';
  let mockedPutGetResponse: any;
  let mockedCopyDeleteResponse: any;

  beforeEach(() => {
    process.env.AWS_XRAY_CONTEXT_MISSING = 'LOG_ERROR';
    process.env.IS_OFFLINE = 'true';

    mockedPutGetResponse = {
      $response: {
        nextPage: () => undefined,
        redirectCount: 0,
        requestId: '',
        retryCount: 0,
        data: {},
        error: null,
        hasNextPage: () => false,
        httpResponse: {
          createUnbufferedStream: () => null,
          statusCode: 0,
          statusMessage: '',
          streaming: false,
          headers: {},
          body: ''
        }
      },
      ETag: '',
      Expiration: '',
      RequestCharged: '',
      SSECustomerAlgorithm: '',
      SSECustomerKeyMD5: '',
      SSEKMSKeyId: '',
      ServerSideEncryption: '',
      VersionId: ''
    };

    mockedCopyDeleteResponse = {
      $response: {
        nextPage: () => undefined,
        redirectCount: 0,
        requestId: '',
        retryCount: 0,
        data: {},
        error: null,
        hasNextPage: () => false,
        httpResponse: {
          createUnbufferedStream: () => null,
          statusCode: 0,
          statusMessage: '',
          streaming: false,
          headers: {},
          body: ''
        }
      },
      RequestCharged: '',
      VersionId: ''
    };

    fileName = 'sample';

    // for now it was statically defined here
    s3 = new S3Helper(businessLocationBucket, new AWS.S3({
      s3ForcePathStyle: true,
      endpoint: 'http://localhost:8002'
    }));
    businessLocationDownload = require('./data/test-data.json');
  });

  afterEach(() => {
    process.env.AWS_XRAY_CONTEXT_MISSING = '';
    process.env.IS_OFFLINE = '';

    sandbox.restore();
  });

  it(`should put a file to S3 Bucket, and have an 'ETag' property`, (done) => {
    sandbox.stub(s3, 'put').resolves(mockedPutGetResponse);

    s3.put(`in-progress/${fileName}`, JSON.stringify(businessLocationDownload)).then((result: any) => {
      assert.exists(result.ETag, 'unable to put object to Bucket');
      done();
    }).catch((err: any) => {
      console.log(err);
      done();
    });
  });

  it(`should get a file from S3 Bucket`, (done) => {
    sandbox.stub(s3, 'get').resolves(mockedPutGetResponse);

    s3.get(`in-progress/${fileName}`).then((result: any) => {
      assert.exists(result.Body, 'unable to get the object from Bucket');
      done();
    }).catch((err: any) => {
      console.log(err);
      done();
    });
  });

  it(`should copy a file from a specific key to another key then delete it`, (done) => {
    sandbox.stub(s3, 'copyThenDelete').resolves(mockedCopyDeleteResponse);

    s3.copyThenDelete(`in-progress/sample`, `processed/sample`).then((result: any) => {
      assert.exists(result, 'failed to delete the file');
      done();
    }).catch((err: any) => {
      console.log(err);
      done();
    });
  });
});
