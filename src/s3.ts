import * as AWS from 'aws-sdk';
import { S3 } from 'aws-sdk';
import * as log from 'econ-logger';
const s3 = new AWS.S3();

export class S3Helper {
  public bucketName: string;
  private s3: AWS.S3;

  /**
   * constructor
   * @param s3 AWS.S3 instance
   */
  constructor(bucketName: string, s3 = new AWS.S3()) {
    this.s3 = s3;
    this.bucketName = bucketName;
  }

  /**
   * get - will get the file from S3 Bucket
   * @param key the key string from s3 Bucket
   * @returns a promise, a thenable promise
   */
  public get(key: string) {
    log.info({
      message: `Trying to download json from ${this.bucketName}/${key}`
    });
    let params: AWS.S3.GetObjectRequest = {
      Bucket: this.bucketName,
      Key: key
    };
    return this.s3.getObject(params).promise();
  }

  /**
   * put - will put the file to S3 Bucket
   * @param key the key string from s3 Bucket
   * @param jsonString an array of BusinessLocationDownload
   * @returns a promise, a thenable promise
   */
  public put(key: string, jsonString: string) {
    log.info({
      message: `Trying to upload json to ${this.bucketName}/${key}`
    });
    let params: AWS.S3.PutObjectRequest = {
      Bucket: this.bucketName,
      Key: key,
      ContentType: 'application/json',
      Body: jsonString
    };
    return this.s3.putObject(params).promise();
  }

  /**
   * copyThenDelete
   */
  public async copyThenDelete(oldKey: string, newKey: string) {
    log.info({
      message: `Trying to move the file ${oldKey} to ${newKey}`
    });
    // for copy from source
    let moveParams: AWS.S3.CopyObjectRequest = {
      Bucket: this.bucketName,
      CopySource: `${this.bucketName}/${oldKey}`,
      Key: `${newKey}`
    };

    let copyResult: any = await this.s3.copyObject(moveParams).promise();

    if (copyResult.hasOwnProperty('CopyObjectResult')) {
      // delete old key
      let deleteParams: AWS.S3.DeleteObjectRequest = {
        Bucket: this.bucketName,
        Key: `${oldKey}`
      };

      return this.s3.deleteObject(deleteParams).promise();
    } else {
      throw new Error(`cannot move ${this.bucketName}/${oldKey} to ${this.bucketName}/${newKey}`);
    }
  }

/**
 * Lists s3 objects of specified params
 * @param params S3.Types.ListObjectsV2Request
 * @param startKey the last key of the previous listObjectsV2 operation, automatically supplied value
 */
  public async listObjects(params: S3.Types.ListObjectsV2Request, startKey?: string) {
    if (startKey) {
      params['StartAfter'] = startKey;
    }
    let results = await s3.listObjectsV2(params).promise();
    if (results.IsTruncated && params.MaxKeys === undefined) {
      // @ts-ignore
      const result = await this.listObjects(params, results.Contents[results.Contents.length - 1].Key);
      // @ts-ignore
      results.Contents = [ ...results.Contents, ...result.Contents ];
    }
    return results;
  }
}
