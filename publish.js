'strict'
const { promisify } = require('util');

async function getNpm() {
  const npmCli = require('npm');
  const npmLoadAsync = await promisify(npmCli.load);
  const npm = await npmLoadAsync();
  return npm;
}

async function checkDeployedVersion() {
  const npm = await getNpm();
  const npmShowAsync = await promisify(npm.commands.show);
  const versionObject = await npmShowAsync(['tiqqe-s3', 'version']);
  const version = Object.keys(versionObject)[0];
  return version;
}

async function isNewVersion() {
  const deployedVersion = await checkDeployedVersion();
  const localVersion = (require('./package.json')).version;
  console.log(`Deployed version: ${deployedVersion}. Local version: ${localVersion}`);
  return localVersion !== deployedVersion;
}

(async () => {
  if (await isNewVersion()) {
    console.log('Npm publish!');
    let npmLogin = require('npm-cli-login');
    const username = process.env.NPM_USER || '';
    const password = process.env.NPM_PASSWORD || '';
    const email = process.env.NPM_EMAIL || '';

    try {
      const resp = await npmLogin(username, password, email);
      console.log(resp);
    } catch (ex) {
      console.error(ex);
    }
  } else {
    console.log('Do nothing...');
  }
})();